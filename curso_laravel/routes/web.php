<?php

Route::get('/', function () {
   return 'Home';
});

Route::get('/usuarios', 'UserController@index')
    ->name('users.index');


Route::get('/usuarios/{id}', 'UserController@show')
    ->where('id', '[0-9]+')
    ->name('users.show');


Route::get('/usuarios/nuevo', 'UserController@create')->name('users.create');
Route::post('/usuarios', 'UserController@store')->name('users.crear');


Route::get('/profesion/nuevo', 'UserController@crearProfesion')->name('users.crearProfesion');
Route::post('/profesion', 'UserController@storeProfession')->name('users.storeProfession');


Route::get('/usuarios/{user}/editar', 'UserController@edit')->name('users.edit');
Route::put('/usuarios/{user}', 'UserController@update');


Route::delete('/usuarios/{user}', 'UserController@destroy')->name('users.destroy');
