<?php

namespace App\Http\Controllers;

use App\User;
use App\Profession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        $title = 'Listado de usuarios';

        return view('users.index', compact('title', 'users'));
    }

    public function show(User $users, $id)
    {
      $user = User::select('users.*', 'professions.title')
                  ->leftJoin('professions', 'users.profession_id', '=', 'professions.id')
                  ->where('users.id','=',$id)
                  ->get();

        return view('users.show', compact('user'));
    }

    public function create(Profession $profesion)
    {
        $profesion = Profession::all();

        return view('users.create', compact('profesion'));
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => 'required',
            'profesion' => ''
        ], [
            'name.required' => 'El campo nombre es obligatorio'
        ]);

       $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'profession_id' => $request->profesion
        ]);

        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function update(User $user){

      $data = request()->validate([
        'name' => 'required',
        'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
        'password' => ''
      ]);

      if($data['password'] == null){
        $data['password'] = bcrypt($data['password']);
      }else{
        unset($data['password']);
      }

      $user->update($data);

      return redirect()->route('users.show', ['user' => $user]);
    }

    public function destroy(User $user){
      $user->delete();

      return redirect()->route('users.index');
    }

    public function crearProfesion(){
      return view('users.createProfession', compact('profesion'));
    }

    public function storeProfession(Request $request){
      $data = request()->validate([
          'name' => 'required',
      ]);


     $profesion = Profession::create([
          'title' => $data['name']
      ]);

      return redirect()->route('users.index');
    }
}
