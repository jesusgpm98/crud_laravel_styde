@extends('layout')

@section('title', "Crear Profesion")

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-1">
        <br>
        <h1>Crear Profesion</h1>
        <br>
        {{-- @if ($errors->any())
          <div class="alert alert-danger">
            <h6>Por favor corrige los errores:</h6>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif --}}

        <form method="POST" action="{{ route('users.storeProfession') }}">
          {{ csrf_field() }}

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="name">Nombre:</label>
            <div class="col-sm-6">
              <input class="form-control" type="text" name="name" id="name" placeholder="Diseñador Web">
            </div>
          </div>
          <br>

          <div class="row justify-content-between">
            <div class="col-4">
              <a class="btn boton" href="{{ route('users.index') }}">Regresar</a>
            </div>

            <div class="col-6">
              <button type="submit" class="btn boton">Crear</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
