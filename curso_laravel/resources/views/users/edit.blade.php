@extends('layout')

@section('title', "Editar usuario")

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-1">
        <br>
        <h1>Editar usuario</h1>
        <br>

        @if ($errors->any())
          <div class="alert alert-danger">
            <h6>Por favor corrige los errores:</h6>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <form method="POST" class="" action="{{ url("usuarios/{$user->id}") }}">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="name">Nombre:</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="name" id="name" placeholder="Jesus Perez" value="{{ old('name', $user->name) }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="email">Email:</label>
            <div class="col-sm-6">
              <input type="email" class="form-control" name="email" id="email" placeholder="jesus@example.com" value="{{ old('email', $user->email) }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="password">Contraseña:</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="password" id="password" placeholder="Mayor a 6 caracteres">
            </div>
          </div>
          <br>

          <div class="row justify-content-between">
            <div class="col-4">
              <a class="btn boton" href="{{ route('users.index') }}">Regresar</a>
            </div>

            <div class="col-6">
              <button type="submit" class="btn boton">Actualizar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
