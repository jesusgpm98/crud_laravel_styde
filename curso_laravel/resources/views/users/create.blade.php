@extends('layout')

@section('title', "Crear usuario")

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-1">
        <br>
        <h1>Crear usuario</h1>
        <br>
        @if ($errors->any())
          <div class="alert alert-danger">
            <h6>Por favor corrige los errores:</h6>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <form method="POST" action="{{ route('users.crear') }}">
          {{ csrf_field() }}

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="name">Nombre:</label>
            <div class="col-sm-6">
              <input class="form-control" type="text" name="name" id="name" placeholder="Pedro Perez" value="{{ old('name') }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="email">Email:</label>
            <div class="col-sm-6">
              <input class="form-control" type="email" name="email" id="email" placeholder="pedro@example.com" value="{{ old('email') }}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="">Profesion:</label>
            <div class="col-sm-6">
              <select class="browser-default custom-select" name="profesion">
                  <option value="">Seleccione</option>
                @foreach ($profesion as $data)
                  <option value="{{ $data->id }}">{{ $data->title }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="password">Contraseña:</label>
            <div class="col-sm-6">
              <input class="form-control" type="password" name="password" id="password" placeholder="Mayor a 6 caracteres">
            </div>
          </div>
          <br>

          <div class="row justify-content-between">
            <div class="col-4">
              <a class="btn boton" href="{{ route('users.index') }}">Regresar</a>
            </div>

            <div class="col-6">
              <button type="submit" class="btn boton">Crear</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
