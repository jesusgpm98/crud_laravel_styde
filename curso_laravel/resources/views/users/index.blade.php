@extends('layout')

@section('title', 'Usuarios')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <br>
        <h1 class="text-center">Usuarios Ingresados</h1>
        <br>
        <table class="table table-sm">
          <thead>
            <tr>
              <th scope="col" >ID</th>
              <th scope="col" >Nombre</th>
              <th scope="col" >Email</th>
              <th scope="col" >Acciones</th>
            </tr>
          </thead>
          <tbody>
              @forelse ($users as $user)
                <tr>
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                    <form action="{{ route('users.destroy', $user) }}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <a class="icon btn btn-link" title="Ver Detalle" href="{{ route('users.show', $user) }}"><i class="fa fa-eye"/></i></a>
                      <a class="icon btn btn-link" title="Editar" href="{{ route('users.edit', $user) }}"><i class="fa fa-user-edit"/></i></a>
                      <button type="submit" title="Eliminar" class="icon btn btn-link"><i class="fa fa-trash"/></i></button>
                    </form>
                  </td>
                </tr>
            @empty
              <td>No hay usuarios registrados.</td>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('sidebar')
  @parent
@endsection
