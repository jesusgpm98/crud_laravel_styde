@extends('layout')

@section('title', "Usuario {$user[0]->id}")

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <br>

        <h1>Usuario #{{ $user[0]->id }}</h1>

        <table class="table table-bordered">
          <tbody>
            <tr>
              <th>Nombre del usuario: </th>
              <td>{{ $user[0]->name }}</td>
            </tr>
            <tr>
              <th>Correo electrónico:</th>
              <td>{{ $user[0]->email }}</td>
            </tr>
            <tr>
              <th>Profesion:</th>
              <td>{{ $user[0]->title }}</td>
            </tr>
          </tbody>
        </table>
        <p>
          <a class="btn boton" href="{{ route('users.index') }}">Volver</a>
        </p>
      </div>
    </div>
  </div>
@endsection
